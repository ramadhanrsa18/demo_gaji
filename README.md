Panduan Clone :
- Move ke branch Master
- Klik clone pilih Clone with SSH
- Lalu buka command promt pada window dan masuk pada directory xamp/htdocs
- Tuliskan perintah git clone paste url gitlab
- Tekan Enter

Panduan instalasi :
1. Download dan install aplikasi Xampp. di komputer.
2. Ekstrak file aplikasi dan database menggunakan aplikasi winrar.
3. Rename folder file aplikasi menjadi gajikaryawan, lalu copas ke folder htdocs.
4. Aktifkan Apache dan MySQL pada Xampp / Wampserver.
5. Buka browser, lalu buka alamat localhost/phpmyadmin.
6. Buat database baru dengan nama demo_gaji.
7. Import database baru dengan nama demo_gaji.
8. Jalankan project dengan browser ketik localhost/gajikaryawan.
9. Login dengan username : admin, password : admin
10. Selesai.

Fitur Aplikasi : 
- Halaman Frontend
- Login Admin
- Data Karyawan
- Data Pekerjaan
- Data Gaji Karyawan
- Logout
- dan lainnya

Role akses Staf :
- Melihat hasil perhitungan gaji karyawan
- Create data Karyawan, Data Pekerjaan, & Gaji Karyawan
- Input, Update, & Delete informasi karyawan 
- Cetak slip gaji karyawan


Role akses SPV :
- Melihat Data gaji karyawan
- Cetak Slip gaji Karyawan

AKSES LOGIN : 
- Staf payroll :
username : admin
password : admin

- SPV
username : spv
password : spv

Link Download
- XAMPP : https://www.apachefriends.org/download.html
- winrar : https://www.winzip.com/en/pages/download/winzip-v1/?x-target=ppc&promo=ppc&gclid=CjwKCAjwgb6IBhAREiwAgMYKRuF-AO3hJBTCq9SgdI1PsJRcpX4l6xatehae3mgOJj2JF7GEEtSFohoCihcQAvD_BwE

